libComp() {
	if [[ "$1" == "" ]]
	then
		echo "oublie du nom de la lib ( ar rc [NAME] *o *.gch  $1)"
	else
		if [[ "$1" == *".a"* ]]
		then
			if [[ "$1" == "lib"* ]]
			then
				if [ -f main.o]
				then
					rm main.o
				fi
				if [ -f include ]
				then
					echo "access include"
				else
					echo "creation dossier include"
					mkdir include
				fi
				mv *.o include/
				mv *.gch include/
				ar rc $1 include/*.o include/*.gch
			else
				echo "lib name must begin with lib"
			fi
		else
			echo "Lib Name should contain .a"
		fi
	fi
}

FastPush() {
	if [[ "$1" == "" ]]
	then
		echo "oublie du commentaire (git commit -m [MESSAGE])"
	else
		git add *[.c/.h]
		git commit -m "$1"
		git push
	fi
}

CompNorm() {
	var=$(norminette *.c *.h)
	bool=0
	if [[ $var == *"Error "* ]]
	then
		bool=0
		echo "$var"
	else
		bool=1
	fi

	if [[ $bool == 1 ]]
	then
		comp
		echo "NO NORME ERROR (*.c/*.h)"
	fi
}

CompWithLib() {
	clear
	var=$(norminette *.c *.h)
	bool=0
	if [[ $var == *"Error "* ]]
		then
		bool=0
		echo "$var"
		else
		bool=1
		fi

	if [[ $bool == 1 ]]
		then
		if [[ "$1" == "" ]]
		then
			echo "need lib name W/ lib (exemple : libft -> compL ft)"
		else
			gcc -Wall -Werror -Wextra *.c *.h -L. -l$1
			echo "NO NORME ERROR"
		fi
	fi
}

alias la="ls -a"
alias norm="clear ; norminette *[.c/.h]"
alias comp="clear ; gcc -Wall -Werror -Wextra *.c *.h"
alias compN=CompNorm
alias compE="clear ; gcc -Wall -Werror -Wextra *.c *.h ; ./a.out"
alias compO="gcc -Wall -Werror -Wextra -c *.c *.h"
alias SClean="rm *.o ; rm *.gch ; rm *.c~ ; rm *.h~"
alias creatlib="SClean ; compO ; libComp"
alias Fpush=FastPush
alias compL=CompWithLib
