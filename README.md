# zshrc
Fichier a ajouter dans le repertoire personnel


Commande dispo :

la                    => ls -a

norm                  => clear ; norminette *[.c/.h]

comp                  => clear ; gcc -Wall -Werror -Wextra *c *h

compE                 => comp ; ./a.out

compN                 => compile si la norme passe, sinon, renvoie les erreurs de norme

compO                 => creer des objets de touts les .c et .h

SClean                => supprime les fichier .o, .gch, .c~ et .h~

creatlib [libname]    => creation de librairie (doit commencer par lib et finir par .a (libft.a))

Fpush [message]       => git add * + git commit -m [message] + git push

compL [libname]       => compile avec la lib, si il n'y a pas d'erreur de norme (libname sans lib: libft -> ft)
